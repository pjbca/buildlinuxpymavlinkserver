#!/bin/bash

# Copyright Peter Burke 2/20/2020

# define functions first


function installstuff {
    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "Installing a whole bunch of packages..."

    start_time_installstuff="$(date -u +%s)"

    time sudo apt-get -y update # 1 min   #Update the list of packages in the software center                                   
    time sudo apt-get -y upgrade # 3.5 min

    time sudo apt-get -y install tcptrack # 0 min
    time sudo apt-get -y install wget # 0 min

    # Python 3 now                                                                                                                              
    
    
    echo "4. Setting up Python virtual environment"
    sudo apt-get  -y install python3
    sudo apt-get  -y install python3-venv
    mkdir ~/ENV
    python3 -m venv ~/ENV
    source ~/ENV/bin/activate
    # These dependencies are required for pymavlink
    sudo apt-get  -y install libxml2-dev libxslt-dev python3-dev
    pip3 install -r https://gitlab.com/pjbca/buildlinuxpymavlinkserver/-/raw/master/requirements.txt     --no-cache-dir
    # Done Python 3    

    time sudo apt-get -y install emacs # 4.5 min  (seems you have to run this twice)                                             
    time sudo apt-get -y install emacs # 0 min (seems you have to run this twice)                                             


    echo "Done installing a whole bunch of packages..."


    end_time_installstuff="$(date -u +%s)"
    elapsed_installstuff="$(($end_time_installstuff-$start_time_installstuff))"
    echo "Total of $elapsed_installstuff seconds elapsed for installing packages"
    # 38 mins
    
    
}

# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}



function fxyz {
    echo "doing function fxyz"
}




#***********************END OF FUNCTION DEFINITIONS******************************

set -x

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "This will install mavlink router and set it up for you."
echo "See README in 4guav gitlab repo for documentation."


echo "Starting..."

date


start_time="$(date -u +%s)"


installstuff


date

end_time="$(date -u +%s)"

elapsed="$(($end_time-$start_time))"
echo "Total of $elapsed seconds elapsed for the entire process"


echo "Installation is complete. You will want to restart your Pi to make this work."
echo "No further action should be required. Closing..."

